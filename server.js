let express = require('express');
let bodyParser = require('body-parser');

let app = express();

let port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/message', (req, res) => {
  res.json({message: 'Nobody\'s fault but mine ... atualizando ...'});
});

let distDir = __dirname + "/webapp/";
app.use(express.static(distDir));

let server = app.listen(port, () => {
  console.log("Appication started. Listening on port %s", server.address().port);
});

module.exports = server
